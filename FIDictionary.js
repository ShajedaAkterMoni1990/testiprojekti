
  let dictionary = [];
  const express = require("express");
  const app = express();
  const fs = require('fs');
  const path = require("path");

  app.use(express.json());
  app.use(express.urlencoded({extended: true}));
  app.use(express.text());
  
 /*CORS isn’t enabled on the server, this is due to security reasons by default,
so no one else but the webserver itself can make requests to the server.*/
// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  res.setHeader("Content-type", "application/json");

  // Pass to next layer of middleware
  next();
});

 
  
  const data =   fs.readFileSync('./sanakirja.txt', {encoding: 'utf8', flag: 'r'});
  const myArr = data.split(/\r?\n/);
  
  
  app.get("/sanakirja",(req,res) => {
      
     
    //data:ssa on nyt koko tiedoston sisältö
   
   // const myArr = data => data.split(/\r?\n/);
  
   // tässä voisi käydä silmukassa läpi splilines:ssa jokaisen rivin
   myArr.forEach((line) => {
       const words = line.split(" ");
        // sanat taulukkoon words
       const word = {
          fin:words[0],
          eng:words[1]
        };
  
          dictionary.push(word);
         // console.log(dictionary);
   })
  
  
    res.json(myArr);
    
  });
  
  function searchEng(haeSana){
      var eng = "";
      try{
          myArr.forEach((line) => {
             const sana = line.split( " ");
              var sana2 = sana[0];
              var sana3 = sana[1];
              if(haeSana === sana2 ){
                  eng = sana3;
              }
          } );
      }catch (err){
          console.error(err);
      }
      return eng;
  }
  app.post("/sanakirja", (req, res) => {
    const data = req.body;
    const data1 = JSON.stringify(data);
   const datas = JSON.parse(data1);
   let newword = (datas.fin+ " " + datas.eng+"\n");
    fs.writeFileSync('sanakirja.txt', newword,  {flag:'a+'});

    console.log(fs.readFileSync("sanakirja.txt","utf8"));
    console.log(dictionary);
    res.json(dictionary);
   // const data = JSON.stringify(dat);
    //const palaute = JSON.parse(data);
   /* if (!fs.existsSync("sanakirja.txt")) {
      fs.writeFile(
        path.join(__dirname, "/sanakirja.txt"),
        data + "\n",
        function (err) {
          if (err) return console.log("Virhe");
        }
      );
    } else {
      fs.appendFile(
        path.join(__dirname, "/sanakirja.txt"),
        data + "\r",
        function (err) {
          if (err) return console.log("Virhe\n");
        }
      );
    }
    res.json(data);*/
  });
  app.get("/sanakirja/:fin", (req, res) => {
      var searchWord = String(req.params.fin);
      res.json(searchEng(searchWord));
  });


  app.listen(3001,  () => {
      console.log("Server is listening on port 3001");})