// Add headers
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
extended: true
}));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");
  
    // Request methods you wish to allow
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
  
    // Request headers you wish to allow
    res.setHeader(
      "Access-Control-Allow-Headers",
      "X-Requested-With,content-type"
    );
  
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);
  
    // Pass to next layer of middleware
    next();
  });

  
  // First you need to create a connection to the database
  // Be sure to replace 'user' and 'password' with the correct values
  const con = mysql.createConnection({
    host: "localhost",
    user: "Shajeda",
    password: "kt123456",
    database: "puhelinluettelo",
    multipleStatements: true, //out parametria varten aliohjelmassa
  });
  
  con.connect((err) => {
    if (err) {
      console.log("Error connecting to Db");
      return;
    }
    console.log("Connection established");
  });
 
  app.get('/',function(req,res){

  con.query("SELECT * FROM henkilot", (err, rows) => {
        if (err) throw err;
        console.log("Data received from Db:");
        rows.forEach((row) => {
          console.log(`${row.nimi}, puhelin on ${row.puhelin}`);
        });
      
         res.json(rows);

    });
    
  });

  app.get('/:id',function(req,res){
      let user_id = req.params.id;

      if (!user_id) {
        return res.status(400).send({ error: true, message: 'Please provide user_id' });
        }
    con.query("SELECT * FROM henkilot where id=?", user_id,(err, rows) => {
          if (err) throw err;
          console.log("Data received from Db:");
          rows.forEach((row) => {
            console.log(`${row.nimi}, puhelin on ${row.puhelin}`);
          });
        
           res.json(rows);
  
      });
      
    });
 
  
    app.put('/', function (req, res) {
        let id = req.body.id;
        let nimi = req.body.nimi;
        let puhelin = req.body.puhelin;
        if (!id || !nimi || !puhelin) {
        return res.status(400).send({ error: nimi, message: 'Please provide the name and user_id' });
        }
    con.query("UPDATE henkilot SET nimi = ? , puhelin = ? WHERE id = ?", [nimi, puhelin, id], function (error, results, fields) {
        if (error) throw error;
        
        res.json(nimi);
         });
     });


     app.post('/', function(req,res){
       let nimi = req.body.nimi;
       let id = req.body.id;
       let puhelin = req.body.puhelin;

       if(!nimi || !id|| !puhelin){
         return res.status(400).send({error: nimi, message: ' Please provide user properly'});
       }

       con.query("INSERT INTO henkilot SET ?", {nimi:nimi, id:id, puhelin:puhelin}, function(error, results, fields ){
         if(error) throw error;

         res.json(id);
       });
     });

     app.delete('/',function(req,res){
       let id = req.body.id;

       if(!id){
         return res.status(400).send({error: nimi, message: 'Please provide right id'});
         }
         con.query('DELETE FROM henkilot WHERE id = ?', [id], function(error, results, fields){
           if(error) throw error;
           res.json(id);
         });
     });
  
  // const henkilo = { nimi: 'Ankka Roope', puhelin: '050-1231232' };
  // con.query('INSERT INTO henkilot SET ?', henkilo, (err, res) => {
  //   if(err) throw err;
  
  //   console.log('Last insert ID:', res.insertId);
  // });
  
  // con.query(
  //     'UPDATE henkilot SET puhelin = ? Where ID = ?',
  //     ['044-6544655', 3],
  //     (err, result) => {
  //       if (err) throw err;
  
  //       console.log(`Changed ${result.changedRows} row(s)`);
  //     }
  //   );
  
  // con.query("DELETE FROM henkilot WHERE id = ?", [5], (err, result) => {
  //   if (err) throw err;
  
  //   console.log(`Deleted ${result.affectedRows} row(s)`);
  // });
  
   con.query("CALL sp_get_henkilot()", function (err, rows) {
    if (err) throw err;
  
    rows[0].forEach( (row) => {
      console.log(`${row.nimi},  puhelin: ${row.puhelin}`);
    });
     console.log(rows);
   });
  
  // con.query("CALL sp_get_henkilon_tiedot(1)", (err, rows) => {
  //   if (err) throw err;
  
  //   console.log("Data received from Db:\n");
  //   console.log(rows[0]);
  // });
  
  // con.query(
  //     "SET @henkilo_id = 0; CALL sp_insert_henkilo(@henkilo_id, 'Matti Miettinen', '044-5431232'); SELECT @henkilo_id",
  //     (err, rows) => {
  //       if (err) throw err;
  
  //       console.log('Data received from Db:\n');
  //       console.log(rows);
  //     }
  //   );
  // const userSubmittedVariable =
  //   "1"; /*ettÃ¤ kukaan ei voi syÃ¶ttÃ¤Ã¤ tÃ¤tÃ¤:
  // const userSubmittedVariable = '1; DROP TABLE henkilot';*/
  
  // con.query(
  //   `SELECT * FROM henkilot WHERE id = ${mysql.escape(userSubmittedVariable)}`,
  //   (err, rows) => {
  //     if (err) throw err;
  //     console.log(rows);
  //   }
  // );
  
 /* con.end((err) => {
    // The connection is terminated gracefully
    // Ensures all remaining queries are executed
    // Then sends a quit packet to the MySQL server.
  });*/

 

  app.listen(3000, function () {
    console.log('Node app is running on port 3000');
    });

