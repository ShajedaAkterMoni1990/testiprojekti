import "./App.css";
import { useState } from "react";
import Axios from "axios";
//import { Form, Col, Button, Row, Nav } from "react-bootstrap";
//import Container from "react-bootstrap/Container";

function App() {
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [weight, setWeight] = useState(0);
  const [url, setUrl] = useState("");
  const [catagory, setCatagory] = useState("");
  const [achievement, setAchievement] = useState("");

  const [newFirstName, setNewFirstName] = useState("");
  const [newLastName, setNewLastName] = useState("");
  const [newBirthday, setNewBirthday] = useState("");
  const [newWeight, setNewWeight] = useState(0);
  const [newUrl, setNewUrl] = useState("");
  const [newCatagory, setNewCatagory] = useState("");
  const [newAchievement, setNewAchievement] = useState("");

  const [playersList, setPlayersList] = useState([]);
  const addPlayer = () => {
    Axios.post("http://localhost:3001/create", {
      firstname: firstname,
      lastname: lastname,
      birthday: birthday,
      weight: weight,
      url: url,
      catagory: catagory,
      achievement: achievement,
    }).then(() => {
      setPlayersList([
        ...playersList,
        {
          firstname: firstname,
          lastname: lastname,
          birthday: birthday,
          weight: weight,
          url: url,
          catagory: catagory,
          achievement: achievement,
        },
      ]);
    });
  };

  const getPlayers = () => {
    Axios.get("http://localhost:3001/players", {}).then((response) => {
      setPlayersList(response.data);
    });
  };

  const updatePlayerWeight = (ID) => {
    Axios.put("http://localhost:3001/update", {
      firstname: newFirstName,
      lastname: newLastName,
      birthday: newBirthday,
      weight: newWeight,
      url: newUrl,
      catagory: newCatagory,
      achievement: newAchievement,
      ID: ID,
    }).then((response) => {
      setPlayersList(
        playersList.map((val) => {
          return val.ID === ID
            ? {
                ID: ID,
                firstname: newFirstName,
                lastname: newLastName,
                birthday: newBirthday,
                weight: newWeight,
                url: newUrl,
                catagory: newCatagory,
                achievement: newAchievement,
              }
            : val;
        })
      );
    });
  };

  const deletePlayer = (ID) => {
    Axios.delete(`http://localhost:3001/delete/${ID}`).then((response) => {
      setPlayersList(
        playersList.filter((val) => {
          return val.ID !== ID;
        })
      );
    });
  };

  return (
    <div>
      <div
        className="container col-md-4  d-flex align-items-right flex-column m-5 p-4 bg-secondary  text-dark"
        style={{ float: "right" }}
      >
        <label>Firstname:</label>
        <input
          type="text"
          id="inputName"
          onChange={(event) => {
            setFirstName(event.target.value);
          }}
        />

        <label>Lastname:</label>
        <input
          type="text"
          id="inputName"
          onChange={(event) => {
            setLastName(event.target.value);
          }}
        />

        <label>Date of Birth:</label>
        <input
          type="date"
          onChange={(event) => {
            setBirthday(event.target.value);
          }}
        />

        <label>Weight:</label>
        <input
          type="number"
          onChange={(event) => {
            setWeight(event.target.value);
          }}
        />
        <label>Url:</label>
        <input
          type="text"
          onChange={(event) => {
            setUrl(event.target.value);
          }}
        />
        <label>Sports Catagory:</label>
        <input
          type="text"
          onChange={(event) => {
            setCatagory(event.target.value);
          }}
        />
        <label>Achievement</label>
        <input
          type="text"
          onChange={(event) => setAchievement(event.target.value)}
        />
        <button type="button" className="btn btn-info m-1" onClick={addPlayer}>
          {" "}
          Add player
        </button>

        <button onClick={getPlayers} className="btn btn-info m-1">
          Show Players
        </button>
      </div>
      <div
        className=" d-flex align-content-start flex-column"
        style={{ width: "500px" }}
      >
        {playersList.map((val, key) => {
          return (
            <div className="player">
              <div className="bg-dark text-white m-3 p-2">
                <div className="d-flex flex-row ">
                  <label className="p-1"> Firstname : {val.firstname} </label>
                  <input
                    className=" m-2"
                    type="text"
                    onChange={(event) => {
                      setNewFirstName(event.target.value);
                    }}
                  ></input>
                </div>
                <div className="d-flex flex-row">
                  <label className="p-1"> Lastname : {val.lastname}</label>

                  <input
                    className="m-2"
                    onChange={(event) => {
                      setNewLastName(event.target.value);
                    }}
                  ></input>
                </div>
                <div className="d-flex flex-row ">
                  <label className="p-1">Date : {val.birthday}</label>

                  <input
                    className="m-2"
                    type="date"
                    onChange={(event) => {
                      setNewBirthday(event.target.value);
                    }}
                  ></input>
                </div>

                <div className="d-flex flex-row ">
                  <label className="p-1">Weight : {val.weight} </label>

                  <input
                    className="m-2"
                    type="number"
                    onChange={(event) => {
                      setNewWeight(event.target.value);
                    }}
                  ></input>
                </div>
                <div className="d-flex flex-row ">
                  <label className="p-1">
                    {" "}
                    Url: <a href="#"> {val.url}</a>
                  </label>
                  <input
                    className="m-2"
                    type="link"
                    onChange={(event) => {
                      setNewUrl(event.target.value);
                    }}
                  ></input>
                </div>
                <div className="d-flex flex-row ">
                  <label className="p-1"> Catagory : {val.catagory} </label>
                  <input
                    className="m-2"
                    onChange={(event) => {
                      setNewCatagory(event.target.value);
                    }}
                  ></input>
                </div>
                <label>Achievement: {val.achievement}</label>
                <input
                  className="m-2"
                  type="text"
                  onChange={(event) => {
                    setNewAchievement(event.target.value);
                  }}
                />

                <div>
                  <button
                    type="button"
                    className="btn btn-success m-2 p-1"
                    onClick={() => {
                      updatePlayerWeight(val.ID);
                    }}
                  >
                    {" "}
                    Update
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger m-1 p-1 "
                    onClick={() => {
                      deletePlayer(val.ID);
                    }}
                  >
                    {" "}
                    Delete
                  </button>
                </div>
                <div className="d-flex flex-row ">
                  <p> To update, write all the informations to the input </p>
                  </div> 
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
