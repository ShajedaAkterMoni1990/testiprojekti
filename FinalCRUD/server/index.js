const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const db = mysql.createConnection({
  user: "root",
  host: "localhost",
  password: "Mountain2019",
  database: "players",
});

app.post("/create", (req, res) => {
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const birthday = req.body.birthday;
  const weight = req.body.weight;
  const url = req.body.url;
  const catagory = req.body.catagory;
  const achievement = req.body.achievement;

  db.query(
    "INSERT INTO player(firstname,lastname,birthday,weight, url, catagory, achievement) VALUES (?,?,?,?,?,?,?)",
    [firstname, lastname, birthday, weight, url, catagory, achievement],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
});

app.get("/players", (req, res) => {
  db.query("SELECT * FROM player", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.put("/update", (req, res) => {
  const ID = req.body.ID;
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const birthday = req.body.birthday;
  const weight = req.body.weight;
  const url = req.body.url;
  const catagory = req.body.catagory;
  const achievement = req.body.achievement;

  //"UPDATE player SET   firstname = ?,lastname =?,birthday = ?,weight =?, url =?, catagory = ? , achievement= ?  where ID = ?"
  //  [firstname, lastname, birthday, weight, url, catagory, achievement, ID],

  db.query(
    "UPDATE player SET   firstname = ?,lastname =?,birthday = ?,weight =?, url =?, catagory = ? , achievement= ?  where ID = ?",
    [firstname, lastname, birthday, weight, url, catagory, achievement, ID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
        console.log("Updated");
      }
    }
  );
});

app.delete("/delete/:ID", (req, res) => {
  const ID = req.params.ID;

  db.query("DELETE FROM player WHERE ID = ?", ID, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
      console.log("Deleted");
    }
  });
});

app.listen(3001, () => {
  console.log("Yey,my server is running on port 3001");
});
