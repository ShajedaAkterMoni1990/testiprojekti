class Person{
    constructor(etunimi, sukunimi, kutsumanimi, syntymavuosi){
        this.etunimi= etunimi;
        this.sukunimi= sukunimi;

        this.kutsumanimi= kutsumanimi;
        this.syntymavuosi = syntymavuosi;
    }

    greeting(){
        console.log(`Hi! I'm ${this.etunimi}`);
    };
}

class Urheilija extends Person{
    constructor(etunimi, sukunimi, kutsumanimi, syntymavuosi, kuva, omapaino, laji, saavutukset){
      super(etunimi, sukunimi, kutsumanimi, syntymavuosi);
        this.kuva = kuva;
        this.omapaino = omapaino;
        this.laji = laji;
        this.saavutukset= saavutukset;
    }

   

    get kuva(){
        return this._kuva;
    }

    set kuva(newKuva){
        this._kuva= newKuva;
    }
    get omapaino(){
        return this._omapaino;
    }

    set omapaino(newOmapaino){
        this._omapaino= newOmapaino;
    }
    get laji(){
        return this._laji;
    }

    set laji(newLaji){
        this._laji= newLaji;
    }
    get saavutukset(){
        return this._saavutukset;
    }

    set saavutukset(newSaavutukset){
        this._saavutukset= newSaavutukset;
    }

    
}

let person = new Person('Mitchel ', ' Huusko', 'Mikko ',1960, 'Cri');
person.greeting();

let urheilija = new Urheilija('Sakib','Al','Hasan','1987','https://cfwsports.com/shakib-al-hasan/', 65, 'Cricket', 'Gold cup');


urheilija.greeting();

console.log(`I'm  ${urheilija.etunimi} ${urheilija.sukunimi} ${urheilija.kutsumanimi}, ${urheilija.kuva}, got ${urheilija.saavutukset}`)

console.log('I\'m  '+ ' ' +urheilija.etunimi+" "+ urheilija.sukunimi + " "+ urheilija.kutsumanimi+ " "+ urheilija.kuva + " got " + urheilija.saavutukset);
console.log(urheilija.etunimi);





